from sanic import Sanic
from sanic.response import html, file , redirect
import aiofiles
import requests
from config import TOKEN, CHAT_ID, IMAGE_NAME, HOST, PORT
import os


app = Sanic()

@app.route("/", methods=["GET"])
async def get(request):
    async with aiofiles.open('index.html', mode='r') as f:
        template = await f.read()
    return html(template,
                headers={'X-Served-By': 'sanic',
		                'Content-Language': 'en',
                        },status=200)


@app.route('/file')
async def handle_request(request):
    pic = os.getcwd() + "/" + IMAGE_NAME
    return await file(pic)


@app.route('/send_message', methods=["POST"])
async def handle_request(request):
    session = requests.session()
    session.proxies['http'] = 'socks5h://localhost:9050'
    session.proxies['https'] = 'socks5h://localhost:9050'
    url = "https://api.telegram.org/bot{0}/sendMessage".format(TOKEN)
    body ={
            "chat_id": CHAT_ID,
            "text": "from:{0}\nemail:{1}\nsubject:{2}\nmessage:{3}".format(
                request.form["name"][0],
                request.form["email"][0],
                request.form["subject"][0],
                request.form["message"][0]
            )
    }
    session.post(url, body)
    url = app.url_for('get')
    return redirect(url)


if __name__ == '__main__':
    app.run(port=PORT, host=HOST)

